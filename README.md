# command line timer  
  
installs a command line tool called `timer` which counts down from given HH:MM:SS, and then sounds an alarm.   
  
# usage   
  
`$ timer 5` # set timer for 5 seconds. counts down from 00:00:05  
  
`$ timer 5:00` # set timer for 5 minutes.  
  
# expected output  
  
```  
$ timer 5  
Thu Aug  1 09:50:58 MST 2019  
timer ended at 9:51:03 AM  
```  
